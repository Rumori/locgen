from setuptools import setup, find_packages

setup(
    name='LocGen',
    version='0.1.5',
    author='Daniel Langh',
    author_email='daniel@rumori.hu',
    packages=find_packages(),
    url='http://bitbucket.org/Rumori/locgen',
    license='LICENSE',
    install_requires = ["lxml"],
    scripts=['scripts/csv2ioslocs', 'scripts/csv2androidlocs', 'scripts/csv2flexlocs'],
    description='iOS localization file generated from CSV',
    long_description=open('README').read()
)