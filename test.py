import locgen

def main():
    '''
    testing this lib locally
    used for development
    '''
    source_path = locgen.get_absolute_path('./example/example.csv')
    locales = locgen.get_localizations(source_path)
    for key in locales:
        text = locgen.format_android_localization(locales[key])
#        text = locgen.format_flex_localization(locales[key])
#        text = locgen.format_ios_localization(locales[key])
        
        filename = '%s.xml' % key
        
        f = open(filename, 'w')
        f.write(text)
        f.close()
        print 'Generated: %s' % filename


if __name__ == '__main__':
    main()
