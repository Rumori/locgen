
import csv
import os, shutil, sys

from lxml import etree

def get_localizations(source_path):
    '''
    source_path: absolute path to csv file
    returns: locale dictionary
    '''
    
    key_column_index = -1
    
    locale_names = []
    locales = {}
    
    f = open(source_path, 'r')
    reader = csv.reader(f)

    lastkey = None
    for row in reader:
        # get the key
        key = None
        
        if key_column_index == -1:
            for i in range(0, len(row)):
                if row[i] == 'Key':
                    key_column_index = i
                    key = row[i]
                    break
        else:
            key = row[key_column_index]

        key = key.strip()
        if key == '':
            key = None

        # handle header, create locale dictionaries
        if key == 'Key':
            locale_names = row[key_column_index+1:]
            for locale_name in locale_names:
                locales[locale_name] = {}
            continue

        if key is None and lastkey is None:
            continue

        fillarray = (key is None) and (lastkey is not None)

        if key:
            lastkey = key
        else:
            key = lastkey
        
        for i in range(key_column_index+1, len(row)):
            locale_name = locale_names[i - key_column_index-1]
            value = str(row[i])
            locale = locales[locale_name]
            
            if fillarray:
                if type(locale[key]) is not list:
                    locale[key] = [locale[key]]
                locale[key].append(value)
            else:
                locale[key] = value

    f.close()
    
    return locales


def format_ios_localization(locale):
    '''
    locale: the locale dictionary, key-value pairs
    returns: text formatted as iOS localization string file
    '''
    output = ''
    for key in locale:
        value = locale[key]
        
        if type(value) is list:
            raise Exception('Lists are not supported in iOS locale files. At key: %s' % key)
        escaped = value.replace('"', '\\"')
        output += '"%s" = "%s";\n' % (key, escaped)

    return output

def format_flex_localization(locale):
    
    resources = etree.XML('<resources/>')
    bundle = etree.Element('resourceBundle')
    bundle.set('name', "myResources")
    resources.append(bundle)
    
    for key in locale:
        value = locale[key]

        resource = etree.Element('resource')
        resource.set("name", key)
        value = unicode(value, 'utf-8')
        resource.set("value", value)
        bundle.append(resource)

    output = etree.tostring(resources, encoding='UTF-8', pretty_print=True, xml_declaration=True)
    return output

    
def format_android_localization(locale):
    
    resources = etree.XML('<resources/>')
    
    for key in locale:

        value = locale[key]
        
        if type(value) is list:
            resource = etree.Element('string-array')
            resource.set('name', key)
            for item in value:
                subresource = etree.Element('item')
                subresource.text = unicode(item, 'utf-8')
                resource.append(subresource)
            pass
            resources.append(resource)
            
        else:
            resource = etree.Element('string')
            resource.set("name", key)
            resource.text = unicode(value, 'utf-8')
            resources.append(resource)

    output = etree.tostring(resources, encoding='UTF-8', pretty_print=True, xml_declaration=True)
    return output


def get_absolute_path(path):
    '''
    Helper to convert path to absolutepath, including abbreviated path, like home dir
    '''
    path = os.path.expanduser(path)
    path = os.path.abspath(path)
    return path
